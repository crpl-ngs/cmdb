#include <stdint.h>
#include <stdio.h>
#include <string.h>

static size_t mstrlen(const char *s) {
    size_t l = 0;
    while (*(s++)) {
        l++;
    }
    return l;
}

class Sequence {
  public:
    char *s;
    uint64_t l;

    Sequence(char *s, uint64_t l) : s(s), l(l) {}
    Sequence(char *s) : s(s), l(mstrlen(s)) {}

    char &operator[](int64_t idx) { return s[idx]; }
    char &operator[](int64_t idx) const { return s[idx]; }
};

class Cell {
    typedef int64_t cell_unit;

  public:
    cell_unit op, E, F, H;

    Cell(cell_unit op = 0, cell_unit E = 0, cell_unit F = 0, cell_unit H = 0) : op(op), E(E), F(F), H(H) {}
};

class Config {
  public:
    /// the values should be negative for penalties
    int go, ge, ma, mi;
    int band_width;

    Config(int bw = 1024, int go = -6, int ge = -1, int ma = 2, int mi = -1)
        : go(go), ge(ge), ma(ma), mi(mi), band_width(bw) {}
};

/**
 * This is the function expected to be running on GPU
 * ref is put vertically, qry is horizontal
 *
 * The data stored in the tiles are already transformed to integers
 *  starting from 0 to N - 1 where N is the size of the alphabet
 *
 *
 *
 * @param ref       The tile from the reference
 * @param qry       The tile from the query
 * @param dp_table  The alloc'd memory to store the dp_table, which is assumed
 *                  to be zero-ized.
 * @param mat       The scoring matrix
 * @param go        The gap opening penalty to be subtracted (mostly positive)
 * @param ge        The gap extension penalty to be subtracted (mostly positive)
 */

#pragma acc routine
static int maxi(int zero, int E, int F, int H, int *v) {
    if (H >= zero && H >= E && H >= F) {
        *v = H;
        return 3;
    }

    if (E >= zero && E >= F && E >= H) {
        *v = E;
        return 1;
    }

    if (F >= zero && F >= E && F >= H) {
        *v = F;
        return 2;
    }

    if (zero >= E && zero >= F && zero >= H) {
        *v = zero;
        return 0;
    }

    return -1;
}

Cell compute_cell(char r, char q, Cell l, Cell t, Cell lt, Config cfg) {
    int &go = cfg.go;
    int &ge = cfg.ge;
    int &ma = cfg.ma;
    int &mi = cfg.mi;

    Cell cc;
    int to = l.H + go;
    int te = l.E + ge;
    int E = to > te ? to : te;
    E = E > 0 ? E : 0;

    to = t.H + go;
    te = t.F + ge;
    int F = to > te ? to : te;
    F = F > 0 ? F : 0;

    int H = lt.H + (q == r ? ma : mi);

    int v;
    /// E is go horizontally, skip 1 char in query, I
    /// F is go vertically, skip 1 char in reference, D
    /// H is go diagonally, either = or X
    /// chain ends when the next cell is 0
    /// "=IDX"
    int op = maxi(0, E, F, H, &v);
    if (q == r && op == 3) {
        op = 0;
    }
    cc.op = op;
    cc.H = v;
    return cc;
}

bool get_cell(Cell &c, Cell const *dp_table, int64_t qpos, int64_t rpos, uint64_t w, uint64_t h) {
    Cell cc;
    if (qpos < 0 || qpos >= w || rpos < 0 || rpos >= h) {
        c = cc;
        return false;
    } else {
        Cell(*dp)[w] = (Cell(*)[w])dp_table;
        c = dp[rpos][qpos];
        return true;
    }
}

bool compute_banded_cell(Cell &c, Sequence const &ref, Sequence const &qry, Cell *dp_table, Config const &cfg,
                         int64_t qpos, int64_t rpos) {
    Cell cc;
    uint64_t w = qry.l;
    uint64_t h = ref.l;

    Cell l, t, lt;
    if (get_cell(l, dp_table, qpos - 1, rpos, w, h) && get_cell(t, dp_table, qpos, rpos - 1, w, h) &&
        get_cell(lt, dp_table, qpos - 1, rpos - 1, w, h) && get_cell(cc, dp_table, qpos, rpos, w, h)) {
        c = compute_cell(ref[rpos], qry[qpos], l, t, lt, cfg);
        return true;
    } else {
        c = cc;
        return false;
    }
}

/**
 * Compute DP values, and return the maximum score and location (no traceback)
 */
int gpu_dbanded(Sequence const &ref, Sequence const &qry, Cell *dp_table, Config const &cfg, uint64_t &qpos,
                uint64_t &rpos) {
    int bw = cfg.band_width;
    Cell band[bw];

    int64_t dx = -bw / 2;
    int64_t dy = -bw / 2;
    int64_t wf = 0;

    bool test = true;
    while (test) {
        bool cond = false;
        for (int i = 0; i < bw; ++i) {
            int j = wf - i;
            cond = cond || compute_banded_cell(band[i], ref, qry, dp_table, cfg, qpos, rpos);
        }
        test = cond;
    }

    for (int i = 0; i < bw; ++i) {
    }
}

void compute_cell(Sequence ref, Sequence qry, Cell *dp_table, Config cfg, int r, int c) {
    if (r > 0 && r <= ref.l && c > 0 && c <= qry.l) {
        /// Assume the shape of the dp table is (row, col) = (ref.l + 1, qry.l +
        /// 1)
        int &go = cfg.go;
        int &ge = cfg.ge;
        int &ma = cfg.ma;
        int &mi = cfg.mi;

        Cell(*dp)[qry.l + 1] = (Cell(*)[qry.l + 1]) dp_table;
        Cell &cc = dp[r][c];
        int to = dp[r][c - 1].H + go;
        int te = dp[r][c - 1].E + ge;
        int E = to > te ? to : te;
        E = E > 0 ? E : 0;

        to = dp[r - 1][c].H + go;
        te = dp[r - 1][c].F + ge;
        int F = to > te ? to : te;
        F = F > 0 ? F : 0;

        char qc = qry.s[c - 1];
        char rc = ref.s[r - 1];
        int H = dp[r - 1][c - 1].H + (qc == rc ? ma : mi);

        int v;
        /// E is go horizontally, skip 1 char in query, I
        /// F is go vertically, skip 1 char in reference, D
        /// H is go diagonally, either = or X
        /// chain ends when the next cell is 0
        /// "=IDX"
        int op = maxi(0, E, F, H, &v);
        if (qc == rc && op == 3) {
            op = 0;
        }
        cc.op = op;
        cc.H = v;
    }
}

#pragma acc routine
void gpu_normal_kernel(Sequence ref, Sequence qry, Cell *dp_table, Config cfg) {
    Cell(*dp)[qry.l + 1] = (Cell(*)[qry.l + 1]) dp_table;

    for (size_t r = 1; r <= ref.l; ++r) {
        for (size_t c = 1; c <= qry.l; ++c) {
            compute_cell(ref, qry, dp_table, cfg, r, c);
        }
    }
}

#pragma acc routine vector
inline void gpu_wf_kernel(Sequence ref, Sequence qry, Cell *dp_table, Config cfg) {
    int num_wf = (int)ref.l + (int)qry.l + 1;
#pragma acc loop seq
    for (int wf = 2; wf < num_wf; ++wf) {
#pragma acc loop vector
        for (int c = 1; c < wf; c++) {
            int r = wf - c;
            compute_cell(ref, qry, dp_table, cfg, r, c);
        }
    }
}

void cpu_normal_kernel(Sequence ref, Sequence qry, Cell *dp_table, Config cfg) {
    /// Assume the shape of the dp table is (row, col) = (ref.l + 1, qry.l + 1)
    int &go = cfg.go;
    int &ge = cfg.ge;
    int &ma = cfg.ma;
    int &mi = cfg.mi;

    Cell(*dp)[qry.l + 1] = (Cell(*)[qry.l + 1]) dp_table;

    for (size_t r = 1; r <= ref.l; ++r) {
        for (size_t c = 1; c <= qry.l; ++c) {
            Cell &cc = dp[r][c];

            Cell t = dp[r][c - 1];
            int to = t.H + go;
            int te = t.E + ge;
            int E = to > te ? to : te;
            E = E > 0 ? E : 0;

            t = dp[r - 1][c];
            to = t.H + go;
            te = t.F + ge;
            int F = to > te ? to : te;
            F = F > 0 ? F : 0;

            char qc = qry.s[c - 1];
            char rc = ref.s[r - 1];

            t = dp[r - 1][c - 1];
            int H = t.H + (qc == rc ? ma : mi);

            int v;
            /// E is go horizontally, skip 1 char in query, I
            /// F is go vertically, skip 1 char in reference, D
            /// H is go diagonally, either = or X
            /// chain ends when the next cell is 0
            /// "=IDX"
            int op = maxi(0, E, F, H, &v);
            if (qc == rc && op == 3) {
                op = 0;
            }
            cc.op = op;
            cc.H = v;
        }
    }
}

void cpu_wf_kernel(Sequence ref, Sequence qry, Cell *dp_table, Config cfg) {
    /// Assume the shape of the dp table is (row, col) = (ref.l + 1, qry.l + 1)
    int &go = cfg.go;
    int &ge = cfg.ge;
    int &ma = cfg.ma;
    int &mi = cfg.mi;

    Cell(*dp)[qry.l + 1] = (Cell(*)[qry.l + 1]) dp_table;

    int num_wf = (int)ref.l + (int)qry.l + 1;
    for (int wf = 2; wf < num_wf; ++wf) {
#pragma acc loop
        for (int c = 1; c < wf; c++) {
            int r = wf - c;
            if (c <= qry.l && r <= ref.l) {
                Cell &cc = dp[r][c];

                Cell t = dp[r][c - 1];
                int to = t.H + go;
                int te = t.E + ge;
                int E = to > te ? to : te;
                E = E > 0 ? E : 0;

                t = dp[r - 1][c];
                to = t.H + go;
                te = t.F + ge;
                int F = to > te ? to : te;
                F = F > 0 ? F : 0;

                char qc = qry.s[c - 1];
                char rc = ref.s[r - 1];
                t = dp[r - 1][c - 1];
                int H = t.H + (qc == rc ? ma : mi);

                int v;
                /// E is go horizontally, skip 1 char in query, I
                /// F is go vertically, skip 1 char in reference, D
                /// H is go diagonally, either = or X
                /// chain ends when the next cell is 0
                /// "=IDX"
                int op = maxi(0, E, F, H, &v);
                if (qc == rc && op == 3) {
                    op = 0;
                }
                cc.op = op;
                cc.H = v;
            }
        }
    }
}

int main(int argc, char *argv[]) {
    // Sequence ref = "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAATCAAAAACAACAAATCAGGGGGGATGAATCTAC"
    //                "CTAACTTCAAACTATACTACATGGCTCCAGTAAACAAAA";
    // Sequence qry = "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA"
    //                "GTATATGCCCTAGCCAAGGCAATCCTAAGCAAAAACAACAAAGCAGGGGGCATCATGCTAC"
    //                "CTAACTTCAAACTATACTACAGGGCTACAGTAAACAAAACAACATGGTACTAGTACAAAAA"
    //                "CAGACACATAGAAAAATGGAACAGAATAGAGGGCCCA";
    Sequence ref = "AATCCTAATCTCAGGGGGGATGAATCTTTCATGGCTCAATCCTAATCTCAGGGGGGATGAATCTTTCATGGCTCAATCCTAATCTCAGGGGGGATGAAT"
                   "CTTTCATGGCTC";
    Sequence qry = "AATCCTAAGCGCAGGGGGCATCATGCTTTCAGGGCTAAATCCTAAGCGCAGGGGGCATCATGCTTTCAGGGCTAAATCCTAAGCGCAGGGGGCATCATG"
                   "CTTTCAGGGCTA";
    // Sequence ref = "AATCCTAATC";
    // Sequence qry = "AATCCTAAGC";
    // ref.todev();
    // qry.todev();
    Cell *dp = new Cell[(ref.l + 1) * (qry.l + 1)];
    memset(dp, 0, (ref.l + 1) * (qry.l + 1) * sizeof(Cell));

    Config cfg;
    // printf("ref.s: %p\nqry.s: %p\ndp: %p\n", ref.s, qry.s, dp);

#pragma acc parallel loop copyin(ref, qry, ref.s[ref.l], qry.s[qry.l], cfg) copyout(dp[(ref.l + 1) * (qry.l + 1)])
    for (size_t i = 0; i < 3000000; ++i) {
        // printf("ref.s: %p\nqry.s: %p\ndp: %p\n", ref.s, qry.s, dp);
        // gpu_normal_kernel(ref, qry, dp, cfg);
        cpu_normal_kernel(ref, qry, dp, cfg);
    }

    for (int r = 0; r < ref.l; ++r) {
        for (int c = 0; c < qry.l; ++c) {
            printf("%-2d ", dp[r * (qry.l + 1) + c].H);
        }
        printf("\n");
    }

    printf("ref.l:%ul, qry.l:%ul\n", ref.l, qry.l);

    delete[] dp;
    return 0;
}
