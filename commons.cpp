#include "commons.hpp"
#include <assert.h>
/**
 * This is the function expected to be running on GPU
 * ref is put vertically, qry is horizontal
 *
 * The data stored in the tiles are already transformed to integers
 *  starting from 0 to N - 1 where N is the size of the alphabet
 *
 *
 *
 * @param ref       The tile from the reference
 * @param qry       The tile from the query
 * @param dp_table  The alloc'd memory to store the dp_table, which is assumed
 *                  to be zero-ized.
 * @param mat       The scoring matrix
 * @param go        The gap opening penalty to be subtracted (mostly positive)
 * @param ge        The gap extension penalty to be subtracted (mostly positive)
 */
#pragma acc routine
int maxi(int zero, int E, int F, int H, int *v) {
    if (H >= zero && H >= E && H >= F) {
        *v = H;
        return 3;
    }

    if (E >= zero && E >= F && E >= H) {
        *v = E;
        return 1;
    }

    if (F >= zero && F >= E && F >= H) {
        *v = F;
        return 2;
    }

    if (zero >= E && zero >= F && zero >= H) {
        *v = zero;
        return 0;
    }

    return -1;
}
