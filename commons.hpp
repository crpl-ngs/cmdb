#include <stdint.h>
#include <stdio.h>
#include <string.h>

#pragma acc routine
static size_t mstrlen(const char *s) {
    size_t l = 0;
    while (*(s++)) {
        l++;
    }
    return l;
}

class Sequence {
  public:
    char *s;
    uint64_t l;

    Sequence(char *s, uint64_t l) : s(s), l(l) {}
    Sequence(char *s) : s(s), l(mstrlen(s)) {}

    char &operator[](int64_t idx) { return s[idx]; }
    char &operator[](int64_t idx) const { return s[idx]; }
};

class Cell {
    typedef int64_t cell_unit;

  public:
    cell_unit op, E, F, H;

    Cell(cell_unit op = 0, cell_unit E = 0, cell_unit F = 0, cell_unit H = 0) : op(op), E(E), F(F), H(H) {}
};

class Row {
    int w;
    Cell *data;

  public:
    Row(int w, Cell *data) : w(w), data(data) {}
    Cell &operator[](int idx) { return data[idx]; }
    Cell operator[](int idx) const { return data[idx]; }
};

struct Table {
    int w, h;
    Cell *data;
#pragma acc policy < dp > copyin(data[:w * h])
    Table(int w, int h, Cell *data) : w(w), h(h), data(data) {}
    Row operator[](int idx) const { return Row(w, data + idx * w); }
};

class Config {
  public:
    /// the values should be negative for penalties
    int go, ge, ma, mi;
    int band_width;

    Config(int bw = 1024, int go = -6, int ge = -1, int ma = 2, int mi = -1)
        : go(go), ge(ge), ma(ma), mi(mi), band_width(bw) {}
};

template <typename T> struct Point { T x, y; };

template <typename T> Point<T> make_point(T x, T y) { return {x, y}; }

#pragma acc routine
int maxi(int zero, int E, int F, int H, int *v);
