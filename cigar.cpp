#include "cigar.hpp"

#include <cstdio>
#include <cstring>

inline static cigar::OP get_op(uint32_t v) {
    const uint32_t mask = 0x3;
    return static_cast<cigar::OP>(v & mask);
}

uint32_t cigar::Cigar::put(cigar::OP op) {
    if (l == 0) {
        uint32_t v = 1 << 2 + static_cast<uint32_t>(op);
        ops[l++] = v;
        return v;
    } else {
        auto last_op = get_op(ops[l - 1]);
        if (last_op == op) {
            ops[l - 1] += 1 << 2;
        } else {
            ops[l++] = (1 << 2) + static_cast<uint32_t>(op);
        }
        return ops[l - 1];
    }
}

char *cigar::Cigar::to_str() {
    const uint32_t mask = 0x3;
    char *buf = new char[10 * l + 1];
    strcpy(buf, "");
    for (size_t i = 0; i < l; ++i) {
        sprintf(buf, "%s%d%c", buf, ops[i] >> 2, "=IDX"[ops[i] & mask]);
    }
    return buf;
}
