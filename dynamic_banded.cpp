#include "dynamic_banded.hpp"
#include <vector>

using namespace dynamic_banded;

int pt2band_id(point pt) {
    int id = (pt.x + pt.y) % 3;
    return id;
}

Row band_from_pt(Bands const &bands, point pt) {
    int band_id = (pt.x + pt.y) % 3;
    return bands[band_id];
}

Cell get_cell(Bands const &bands, point pt) {
    if (pt.x <= 0 || pt.y <= 0) {
        Cell c;
        return c;
    }

    int id = pt2band_id(pt);
    auto center = bands.centers[id];
    int64_t x = center.x;
    int bw = bands.band_width;
    if (pt.x < x - bw / 2 || pt.x >= x + (bw + 1) / 2) {
        Cell c;
        return c;
    } else {
        return bands[id][pt.x - x + bw / 2];
    }
}

/**
 *  Get point from band index, band center and band width
 **/
point get_point(point center, int bw, int idx) {
    int64_t x = idx + center.x - bw / 2;
    int64_t y = center.x + center.y - x;
    return make_point(x, y);
}

#pragma acc routine
Cell compute_cell(char r, char q, point center, int i, Bands const &bands, Config const &cfg) {
    int bw = bands.band_width;
    int64_t x = center.x - bw / 2 + i;
    int64_t y = center.x + center.y - x;
    point cur = make_point(x, y);

    if (x <= 0 || y <= 0) {
        Cell c;
        if (x <= 0 && y) {
            c.op = 2;
        } else if (y <= 0 && x) {
            c.op = 1;
        }

        return c;
    }

    Cell l = get_cell(bands, make_point(x - 1, y));
    Cell t = get_cell(bands, make_point(x, y - 1));
    Cell lt = get_cell(bands, make_point(x - 1, y - 1));

    int go = cfg.go;
    int ge = cfg.ge;
    int ma = cfg.ma;
    int mi = cfg.mi;

    Cell cc;
    int to = l.H + go;
    int te = l.E + ge;
    int E = to > te ? to : te;
    E = E > 0 ? E : 0;

    to = t.H + go;
    te = t.F + ge;
    int F = to > te ? to : te;
    F = F > 0 ? F : 0;

    int H = lt.H + (q == r ? ma : mi);

    int v;
    /// E is go horizontally, skip 1 char in query, I
    /// F is go vertically, skip 1 char in reference, D
    /// H is go diagonally, either = or X
    /// chain ends when the next cell is 0
    /// "=IDX"
    int op = maxi(0, E, F, H, &v);
    if (q == r && op == 3) {
        op = 0;
    }
    cc.op = op;
    cc.H = v;
    return cc;
}

struct Result {
    point pt;
    int64_t score;
};

void test(char r, char q, point center, int i, Bands const &bands, Config const &cfg) {}

#pragma acc routine
Result compute_kernel(Sequence &ref, Sequence &qry, Bands &bands, Config const &cfg) {
    uint64_t num_wf = ref.l + qry.l + 1;
    auto center = make_point<int64_t>(0, 0);
    point max_pt;
    int64_t max_score = 0;

    for (uint64_t wf = 0; wf < num_wf; ++wf) {
        Row band = bands[wf % 3];
        bands.centers[wf % 3] = center;

        // #pragma acc loop independent
        for (int i = 0; i < bands.band_width; ++i) {
            point pt = get_point(center, bands.band_width, i);
            if (pt.x < 0 || pt.y < 0 || pt.x >= qry.l || pt.y >= ref.l) {
                band[i] = Cell();
            } else {
                band[i] = compute_cell(ref[pt.y], qry[pt.x], center, i, bands, cfg);
                if (band[i].H > max_score) {
                    max_score = band[i].H;
                    max_pt = pt;
                }
            }
        }
        if (band[0].H < band[bands.band_width - 1].H) {
            /// move right
            center = make_point(center.x + 1, center.y);
        } else {
            /// move down
            center = make_point(center.x, center.y + 1);
        }
    }
    return {max_pt, max_score};
}

int main(int argc, char const *argv[]) {
    using namespace dynamic_banded;
    auto bands = init_memory(32);

    auto ref_cstr = strdup("AATCCTAATCTCAGGGGGGATGAATCTTTCATGGCTCAATCCTAATCTCAGGGGGGATGAATCTTTCATGGCTCAATCCTAATCTCAGGGG"
                           "GGATGAATCTTTCATGGCTC");
    auto qry_cstr = strdup("AATCCTAAGCGCAGGGGGCATCATGCTTTCAGGGCTAAATCCTAAGCGCAGGGGGCATCATGCTTTCAGGGCTAAATCCTAAGCGCAGGGG"
                           "GCATCATGCTTTCAGGGCTA");
    // auto ref_cstr = strdup("AATCC");
    // auto qry_cstr = strdup("AATCC");
    Sequence ref = ref_cstr;
    Sequence qry = qry_cstr;
    // printf("ref: %p, qry: %p, bands: %p\n", ref.s, qry.s, bands.t.data);

#define NUM_PAIRS 300
    auto all_bands = std::vector<Bands>(NUM_PAIRS, bands);
    auto p = all_bands.data();
    Result answers[NUM_PAIRS];

#pragma acc parallel loop copy(p <dp>[:NUM_PAIRS])
    for (size_t i = 0; i < NUM_PAIRS; ++i) {
        // answers[i] = compute_kernel(ref, qry, p[i], Config());
    }

    for (auto ans : answers) {
        printf("Pt: (%ld, %ld), score: %ld\n", ans.pt.x, ans.pt.y, ans.score);
    }

    free(ref_cstr);
    free(qry_cstr);
    destroy_bands(bands);
    return 0;
}
