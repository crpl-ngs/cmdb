#include <cstdint>

namespace cigar {

/// "=IDX"
enum OP { MATCH, INSERTION, DELETION, MISMATCH };

struct Cigar {
    const uint32_t size;
    uint32_t *ops, l;
    bool own;
    Cigar(uint32_t size) : size(size), l(0), own(true) { ops = new uint32_t[size]; }
    Cigar(uint32_t size, uint32_t *ops, uint32_t l) : size(size), l(l), own(false), ops(ops) {}
    uint32_t put(OP op);
    char *to_str();
    ~Cigar() {
        if (own) {
            delete[] ops;
        }
    }
};

} // namespace cigar
