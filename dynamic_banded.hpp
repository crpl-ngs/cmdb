#include "commons.hpp"
#include <iostream>
#include <utility>

namespace dynamic_banded {

typedef Point<int64_t> point;

struct Bands {
    Table t;
    point centers[3]; //  (xcol, yrow)
    int band_width;
#pragma acc policy < dp > copy(t <dp>)
    Bands(Cell *data, int band_width) : t(band_width, 3, data), band_width(band_width) {}
    Row operator[](int idx) { return t[idx]; }
    Row operator[](int idx) const { return t[idx]; }
};

Bands init_memory(int band_width) {
    Cell *data = new Cell[band_width * 3];
    auto bands = Bands(data, band_width);
    for (auto &center : bands.centers) {
        center = make_point<int64_t>(0, 0);
    }
    return bands;
}

void destroy_bands(Bands &bands) { delete[] bands.t.data; }

} // namespace dynamic_banded
