#include "commons.hpp"

namespace gact {

struct Aligner {
    const uint32_t tile_size;
    const int m, mm, go, ge;
    Aligner(uint32_t tile_size, int m = 1, int mm = -4, int go = -6, int ge = -1)
        : tile_size(tile_size), m(m), mm(mm), go(go), ge(ge) {}
};

} // namespace gact
