#include "../cigar.hpp"
#include "catch.hpp"
#include <cstring>

TEST_CASE("cigar", "[cigar]") {
    cigar::Cigar c(10);
    c.put(static_cast<cigar::OP>(0));
    c.put(static_cast<cigar::OP>(0));
    c.put(static_cast<cigar::OP>(0));
    c.put(static_cast<cigar::OP>(1));
    c.put(static_cast<cigar::OP>(1));
    c.put(static_cast<cigar::OP>(2));
    c.put(static_cast<cigar::OP>(3));
    c.put(static_cast<cigar::OP>(3));
    c.put(static_cast<cigar::OP>(2));
    c.put(static_cast<cigar::OP>(1));

    auto s = c.to_str();
    printf("%s\n", s);
    CHECK(!strcmp(s, "3=2I1D2X1D1I"));
    delete[] s;
}